package nl.weeronline.common

import kotlinx.serialization.Optional
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class WeatherAdvice(
    @SerialName("score")
    @Optional
    val score: Int? = null,
    @SerialName("replacements")
    val replacements: Replacements?,
    @SerialName("animation")
    val animation: Int,
    @SerialName("link")
    val link: String,
    @SerialName("message")
    internal val _message: String,
    @SerialName("text")
    @Optional
    val text: String? = null,
    @Optional
    @SerialName("title")
    val title: String? = null,
    @SerialName("full_text")
    @Optional
    val fullText: String? = null
) {

    @Transient
    val message: String
        get() {
            return _message.replace("start_time_utc", "startTime")
                .replace("stop_time_utc", "stopTime")
                .replace("end_of_forecast_time_utc", "endOfForecastTime")
                .replace("symbol_text", "symbolText")
                .replace("day_part_text", "dayPartText")
        }

    @Serializable
    data class Replacements(
        @Optional
        val start_time_utc: String? = null,
        @Optional
        val stop_time_utc: String? = null,
        @Optional
        val end_of_forecast_time_utc: String? = null,
        @Optional
        val symbol_text: String? = null,
        @Optional
        val day_part_text: String? = null
    )
}

@Serializable
internal data class WeeronlineResponse(
    val weeronline: Weeronline
)

@Serializable
internal data class Weeronline(
    val data: WeatherAdvice
)