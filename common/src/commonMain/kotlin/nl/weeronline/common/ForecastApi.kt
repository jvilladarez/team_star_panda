package nl.weeronline.common

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.request.get
import io.ktor.client.request.url
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.JSON

internal expect val ApplicationDispatcher: CoroutineDispatcher

class ForecastApi {

    private val client by lazy {
        HttpClient {
            install(JsonFeature) {
                serializer = KotlinxSerializer(JSON.nonstrict).apply {
                    setMapper(WeatherAdvice::class, WeatherAdvice.serializer())
                    setMapper(WeatherAdvice.Replacements::class, WeatherAdvice.Replacements.serializer())
                    setMapper(Weeronline::class, Weeronline.serializer())
                    setMapper(WeeronlineResponse::class, WeeronlineResponse.serializer())
                }
            }
        }
    }

    fun getWeatherAdvice(cityId: Int, callback: (WeatherAdvice) -> Unit) {
        GlobalScope.apply {
            launch(ApplicationDispatcher) {
                val result = client.get<WeeronlineResponse> {
                    url("https://service.weeronline.cloud/notification/v1/1/1/nl/$cityId")
                }
                callback(result.weeronline.data)
            }
        }
    }
}