//
//  ViewController.swift
//  KotlinFram
//
//  Created by Eduardo Irias on 12/19/18.
//  Copyright © 2018 Weeronline. All rights reserved.
//

import UIKit
import common

class ViewController: UIViewController {

    var weatherAdvice: common.WeatherAdvice!
    
    @IBOutlet var weatherTextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let forecastApi = ForecastApi()
        forecastApi.getWeatherAdvice(cityId: 4058223) { (weatherAdivce) -> KotlinUnit in
            
            self.weatherTextLabel.text = weatherAdivce.message.replacingOccurrences(of: "{dayPartText}", with: weatherAdivce.replacements?.day_part_text ?? "")
            
            return KotlinUnit()
        }
        
    }


}

