package nl.weeronline.wackywedneday

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import nl.weeronline.common.ForecastApi

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val forecastApi = ForecastApi()
        forecastApi.getWeatherAdvice(4058223) {
            runOnUiThread {
                test.text = it.message
            }
        }
    }
}
